#!python3
from random import randint
from typing import Dict, List, Optional, Tuple


class IllegalSize(BaseException):
    pass


class IllegalToken(BaseException):
    pass


class IllegalMove(BaseException):
    pass


class Player:
    def __init__(self, player_token: int, name: Optional[str] = None):
        self.token = player_token
        self.moves: Dict = {}
        self.name = name

    def __str__(self):
        return f"{self.token}{' ' + self.name if self.name else ''}"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.token == other.token


class Board:
    def __init__(
            self, size: int = 3, player_1: Player = Player(1), player_2: Player = Player(2)
    ):
        if not isinstance(size, int):
            raise IllegalSize("Size must be of type int")
        elif size < 3:
            raise IllegalSize("Size must be 3 or more")
        elif not (isinstance(player_1, Player) and isinstance(player_2, Player)):
            raise IllegalToken("Players must be of type Player")
        elif player_1.token == player_2.token:
            raise IllegalToken("Player tokens must be different")
        elif player_1.token == 0 or player_2.token == 0:
            raise IllegalToken("Player tokens cannot be 0")

        self.size = size
        self.board: List[List[int]] = []
        self.board += [[player_1.token] * size]
        for _ in range(size - 2):
            self.board += [[0] * size]
        self.board += [[player_2.token] * size]
        self.player_1 = player_1
        self.player_2 = player_2

    def is_win(self) -> Optional[Player]:
        if self.player_1.token in self.board[-1]:
            return self.player_1
        elif self.player_2.token in self.board[0]:
            return self.player_2
        elif sum(map(lambda x: x.count(self.player_1.token), self.board)) == 0:
            return self.player_2
        elif sum(map(lambda x: x.count(self.player_2.token), self.board)) == 0:
            return self.player_1
        elif not self.moves(self.player_1) and not self.moves(self.player_2):
            return Player(0)
        else:
            return None

    def move(self, from_x: int, from_y: int, to_x: int, to_y: int):
        if (
                self.board[from_y][from_x] != 0
                and self.board[to_y][to_x] == self.board[from_y][from_x]
        ):
            raise IllegalMove("Player cannot eat own tokens")
        elif (
                from_x < 0
                or from_x > self.size - 1
                or from_y < 0
                or from_y > self.size - 1
                or to_x < 0
                or to_x > self.size - 1
                or to_y < 0
                or to_y > self.size - 1
        ):
            raise IllegalMove("Can only move inside the board")
        elif abs(to_y - from_y) > 1 or abs(to_x - from_x) > 1:
            raise IllegalMove("Can only move to adjacent spaces")
        elif self.board[from_y][from_x] == self.player_1.token and to_y <= from_y:
            raise IllegalMove("Players can only move forward")
        elif self.board[from_y][from_x] == self.player_2.token and to_y >= from_y:
            raise IllegalMove("Players can only move forward")

        self.board[to_y][to_x] = self.board[from_y][from_x]
        self.board[from_y][from_x] = 0

    def moves(self, player: Player) -> List[List[int]]:
        player_moves = []
        direction = 1 if player.token == self.player_1.token else -1

        for y, row in enumerate(self.board):
            if y + direction < 0 or y + direction >= self.size:
                continue
            for x, token in enumerate(row):
                if token == player.token:
                    if self.board[y + direction][x] == 0:
                        player_moves.append([x, y, x, y + direction])
                    if x < self.size - 1:
                        if self.board[y + direction][x + 1] not in (0, player.token):
                            player_moves.append([x, y, x + 1, y + direction])
                    if x > 0:
                        if self.board[y + direction][x - 1] not in (0, player.token):
                            player_moves.append([x, y, x - 1, y + direction])

        return player_moves

    def state(self) -> str:
        state_hash = ""
        for y in self.board:
            for x in y:
                state_hash += str(x)

        return state_hash

    def __str__(self):
        string = ""
        for row in self.board:
            string += str(row) + "\n"
        string.strip()

        return string

    def __repr__(self):
        return self.__str__()


class Game:
    def __init__(self, board: Board = Board(), first: int = 0):
        self.board = board
        self.turn = 0
        self.cur_player: Player
        self.history: List[Tuple[str, int, Player, List[int]]] = []

        if first in (0, 1):
            self.cur_player = board.player_1
        elif first == 2:
            self.cur_player = board.player_2
        else:
            raise IllegalToken("First player can only be 1 or 2")

        if self.board.state() not in self.cur_player.moves:
            self.cur_player.moves[self.board.state()] = self.board.moves(self.cur_player)

    def play_auto(self) -> Optional[Player]:
        if self.board.state() not in self.cur_player.moves:
            self.cur_player.moves[self.board.state()] = self.board.moves(self.cur_player)

        moves: List[List[int]] = self.cur_player.moves[self.board.state()]

        if not moves:
            raise IllegalMove("No moves available")

        move = moves[randint(0, len(moves) - 1)]

        self.history.append((self.board.state(), self.turn, self.cur_player, move,))

        self.board.move(*move)

        win = self.board.is_win()

        if win is not None and win.token == 0:
            win = self.cur_player
        elif win is None:
            self.turn += 1
            self.cur_player = (
                self.board.player_2
                if self.cur_player == self.board.player_1
                else self.board.player_1
            )

        return win

    def __str__(self):
        return f"{self.board}\n\n{[self.turn, self.cur_player]}"

    def __repr__(self):
        return self.__str__()


def auto_play(n_games: int, learn: str = ""):
    player_1_main: Player = Player(1, name="Human")
    player_2_main: Player = Player(2, name="Machine")
    wins_1: int = 0
    wins_2: int = 0

    for n in range(n_games):
        print(f"Game #{n + 1}: ", end="", flush=True)

        board = Board(3, player_1_main, player_2_main)
        game = Game(board)
        win: Optional[Player] = None

        while win is None:
            win = game.play_auto()

        print(win)

        player_1_main, player_2_main = game.board.player_1, game.board.player_2

        wins_1 += 1 if win == player_1_main else 0
        wins_2 += 1 if win == player_2_main else 0

        if win == player_1_main:
            if "punishM" in learn and len(player_2_main.moves[game.history[-2][0]]) > 1:
                for i, m in enumerate(player_2_main.moves[game.history[-2][0]]):
                    if m == game.history[-2][3]:
                        player_2_main.moves[game.history[-2][0]] = player_2_main.moves[game.history[-2][0]][:i] + \
                                                                   player_2_main.moves[game.history[-2][0]][i + 1:]
                        break
            if "rewardH" in learn:
                player_1_main.moves[game.history[-1][0]].append(game.history[-1][3])
        elif win == player_2_main:
            if "punishH" in learn and len(player_1_main.moves[game.history[-2][0]]) > 1:
                for i, m in enumerate(player_1_main.moves[game.history[-2][0]]):
                    if m == game.history[-2][3]:
                        player_1_main.moves[game.history[-2][0]] = player_1_main.moves[game.history[-2][0]][:i] + \
                                                                   player_1_main.moves[game.history[-2][0]][i + 1:]
                        break
            if "rewardM" in learn:
                player_2_main.moves[game.history[-1][0]].append(game.history[-1][3])

        del game
        del board

    print(f"Wins player 1: {wins_1}")
    print(f"Wins player 2: {wins_2}")
    print(f"Wins ratio P1: {(wins_1 / n_games) * 100:.2f}%")
    print(f"Wins ratio P2: {(wins_2 / n_games) * 100:.2f}%")
    print()

    for player in (player_1_main, player_2_main):
        print(f"Player {player}")
        for state, moves_main in player.moves.items():
            print([int(e) for e in state[0:3]])
            print([int(e) for e in state[3:6]])
            print([int(e) for e in state[6:9]])
            print("---------")
            print(*moves_main)
            print()


if __name__ == '__main__':
    auto_play(int(input("Number of games: ")), input("Learn mode: [punish/reward][M/H]: "))
